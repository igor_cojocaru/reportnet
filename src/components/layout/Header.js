import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../logo.png';


function Header(props) {
  const { branding } = props;

  return (
    <header className="header_class">
        <nav className=''>
            <img src={logo} className="logo_class" alt="logo" /> 
            <h1>{branding}</h1>
        </nav>
    </header>
  );
}

Header.defaultProps = {
  branding: 'Repotrnet'
};

Header.propTypes = {
  branding: PropTypes.string.isRequired
};

export default Header;
